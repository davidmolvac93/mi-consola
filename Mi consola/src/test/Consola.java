/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author david
 */
public class Consola {

    public static void help(Path ruta2) {

        System.out.println("> Lista de comandos de nuestra CMD para que te sirva de ayuda:  \n"
                + "Para obtener más información acerca de un comando específico, escriba HELP seguido del nombre de comando\n"
                + "PERO DESDE LA PROPIA CMD NO AQUI QUE SE NOS VA DE MADRE, ya seria demasiado pedir hacerlo desde aquí :) \n"
                + "help  ---->  Lista los comandos con una breve definición de lo que hacen \n"
                + "cd  ---->   Muestra el directorio actual \n"
                + "         [..] -> Accede al directorio padre \n"
                + "         [<nombreDirectorio>] -> Accede a un directorio dentro del directorio actual \n"
                + "         [<rutaAbsoluta] -> Accede a la ruta absoluta del sistema \n"
                + "mkdir<nombre_directorio>  ---->  Crea un directorio en la ruta actual \n"
                + "info <nombre>  ---->  Muestra la información del elemento.Indicando FileSystem, Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace \n"
                + "cat <nombreFichero>  ---->  Muestra el contenido de un fichero \n"
                + "top <numeroLineas> <nombreFichero>  ---->  Muestra las líneas especificadas de un fichero \n"
                + "mkfile <nombreFichero> <texto>  ---->  Crea un fichero con ese nombre y el contenido de texto \n"
                + "write <nombreFichero> <texto>  ---->  Añade 'texto' al final del fichero especificado \n"
                + "dir  ---->  Lista los archivos o directorios de la ruta actual \n"
                + "         [<nombreDirectorio>]-> Lista los archivos o directorios dentro de ese directorio \n"
                + "         [<rutaAbsoluta]-> Lista los archivos o directorios dentro de esa ruta \n"
                + "delete <nombre>  ---->  Borra el fichero, si es un directorio borra todo su contenido y a si mismo \n"
                + "close  ---->  Cierra el programa \n"
                + "Clear  ---->  Vacía la vista.");

    }

    public static void cd(String palabra, Path ruta2) {

        System.out.println("Estamos en " + ruta2);
        System.out.println("Ahora podemos hacer estas dos acciones \n"
                + "Si quieres acceder al directorio padre introduce: .. \n"
                + "Si quieres acceder a la ruta absoluta introduce: / ");

        ruta2 = Paths.get(System.getProperty("user.dir"));

        Scanner sc = new Scanner(System.in);
        palabra = sc.next();

        File directorio = new File(ruta2.toString());
        
        if(directorio.exists()){
            
            if(palabra.contentEquals("..")){
                ruta2.getParent();
                System.out.println(ruta2.getParent());
            }
            
            if(palabra.contentEquals("\\") || palabra.contentEquals("/")){
                System.out.println(ruta2.getName(0));
            }else{
                ruta2.resolve(ruta2);
            }
        }
    }

    public static void mkDir(String palabra, Path ruta2) {

        System.out.println("Introduce el nombre del directorio a crear ");

        ruta2 = Paths.get(System.getProperty("user.dir"));
        Scanner sc = new Scanner(System.in);
        palabra = sc.next();

        File directorio = new File(palabra);

        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Directorio creado");
            } else {
                System.out.println("Error al crear directorio");
            }
        } else {
            System.err.print("ERROR ya existe directorio con ese nombre");
        }
    }

    public static void info(Path ruta2) {

        System.out.println("La informacion del elemento es la siguiente: ");
        File fichero = new File(System.getProperty("user.dir"));

        System.out.println(ruta2.getFileSystem());
        System.out.println(ruta2.getParent());
        System.out.println(ruta2.getRoot());
        System.out.println(ruta2.getNameCount());
        System.out.println(fichero.getFreeSpace());
        System.out.println(fichero.getTotalSpace());
        System.out.println(fichero.getUsableSpace());

    }

    public static void cat(String filename) throws FileNotFoundException, IOException {

        System.out.println("Por favor indica el nombre del archivo para poder ver que hay dentro ");

        String archivo;
        Scanner sc = new Scanner(System.in);
        archivo = sc.nextLine();

        File newFile = new File(archivo);
        try {
            if (newFile.exists()) {
                try {
                    String cadena;
                    FileReader f = new FileReader(archivo);
                    BufferedReader b = new BufferedReader(f);
                    while ((cadena = b.readLine()) != null) {
                        System.out.println(cadena);
                    }
                    b.close();
                } catch (IOException ex) {
                    System.out.println("File create exception (Excepción al crear el fichero): \n     " + ex);
                }
            }
        } catch (Exception ex) {
            System.err.print("ERROR INTRODUCE ARCHIVO VALIDO O COMANDO VALIDO");
        }

    }

    public static void top(String filename, String numero) throws IOException {

        System.out.println("Por favor indica el nombre del archivo para poder ver que hay dentro ");

        String archivo;
        String numCadena;
        int numEntero;
        int i = 0;

        Scanner sc = new Scanner(System.in);

        archivo = sc.next();
        numCadena = sc.next();
        numEntero = Integer.parseInt(numCadena);

        File newFile = new File(archivo);

        if (newFile.exists()) {
            try {
                String cadena;
                FileReader f = new FileReader(archivo);
                BufferedReader b = new BufferedReader(f);
                while ((cadena = b.readLine()) != null && i < numEntero) {

                    cadena = b.readLine();
                    i++;
                    System.out.println(cadena);

                }
                b.close();
            } catch (IOException ex) {
                System.out.println("File create exception (Excepción al crear el fichero): \n     " + ex);
            }
        }
    }

    public static void mkFile(String fileName, String[] palabra) {

        System.out.println("Por favor indica el nombre del archivo y el texto a introducir ");

        String archivo;
        Scanner sc = new Scanner(System.in);
        archivo = sc.next();
        String contenido = sc.nextLine();

        boolean result;
        File newFile = new File(archivo);
        if ((result = !newFile.exists())) {
            try {
                //result = newFile.createNewFile();
                //FileWriter fw = new FileWriter(archivo);
                //BufferedWriter bw = new BufferedWriter(fw);
                FileWriter cosa = new FileWriter(archivo);
                cosa.write(contenido);
                cosa.flush();
                cosa.close();
                System.out.println("Archivo actualizado y texto introducido");
            } catch (IOException ex) {
                result = false;
                System.out.println("File create exception (Excepción al crear el fichero): \n     " + ex);
            }
        }
    }

    public static void write(String fileName, String[] palabra) {

        System.out.println("Por favor indica el nombre del archivo y el texto a introducir ");

        String archivo;
        Scanner sc = new Scanner(System.in);
        archivo = sc.next();
        String contenido = sc.nextLine();

        boolean result;
        File newFile = new File(archivo);
        if (newFile.exists()) {
            try {
                //result = newFile.createNewFile();
                //FileWriter fw = new FileWriter(archivo);
                //BufferedWriter bw = new BufferedWriter(fw);
                FileWriter cosa = new FileWriter(archivo, true);
                cosa.write(contenido);
                cosa.flush();
                cosa.close();
                System.out.println("Archivo creado y texto introducido");
            } catch (IOException ex) {
                result = false;
                System.out.println("File create exception (Excepción al crear el fichero): \n     " + ex);
            }
        } else {
            System.out.println("Debes introducir un fichero creado con la finalizacion de .txt");
            write(fileName, palabra);
        }

    }

    public static void dir(String palabra) {

        System.out.println("Introduce la ruta que quieras ver su listado por favor");
        Scanner sc = new Scanner(System.in);
        palabra = sc.next();

        File carpeta = new File(palabra);
        String[] listado = carpeta.list();
        if (listado == null || listado.length == 0) {
            System.out.println("No hay elementos dentro de la carpeta actual");
            return;
        } else {
            for (int i = 0; i < listado.length; i++) {
                System.out.println(listado[i]);
            }
        }
    }

    public static boolean borradoDeDirectorio(File directorio) {

        System.out.println("Vamos a borrar un directorio");

        if (directorio.isDirectory()) {
            String[] children = directorio.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = borradoDeDirectorio(new File(directorio, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        if (directorio.exists()) {
            directorio.delete();
            System.out.println("Has borrado el directorio satisfactoriamente");
            return true;
        } else {
            System.out.println("No se puede borrar, pero no te preocupes todo en la vida tiene solucion \n"
                    + "Introduce la ruta del directorio que deseas borrar sustituyendola por el tp de borrarDirectorio \n"
                    + "asi la podemos borrar");
            return false;
        }

    }

    public static void delete(String fileName) {

        System.out.println("Por favor indica el nombre del archivo a borrar");

        String archivo;
        Scanner sc = new Scanner(System.in);
        archivo = sc.next();

        File newFile = new File(archivo);

        if (newFile.exists()) {
            newFile.delete();
            System.out.println("Hemos borrado el fichero");
        } else {
            System.out.println("Creo que eso no existe bro");
        }
    }

    public static void close(String palabra, Path ruta2) {

        String cerrar;

        System.out.println("Estas seguro de querer cerrar el programa????? \n"
                + "Si deseas cerrarlo introduce la palabra SI");

        Scanner sc = new Scanner(System.in);
        cerrar = sc.next();

        if (cerrar.equalsIgnoreCase("si")) {
            System.out.println("Muchas gracias por utilizar nuestra consola; Saludos");
            System.exit(0);
        } else {
            System.out.println("Recuerda debes introcuir la palabra si para finalizar");
            close(palabra, ruta2);
        }

    }

    public static void clear() {

        System.out.println("Limpieza en curso");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        //Me parece un poco cutre como dices, pero diste el ejemplo y para adelante
    }

}
