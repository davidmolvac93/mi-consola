/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 *
 * @author david
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        Consola prueba = new Consola();
       
        java.nio.file.Path ruta2 = Paths.get(System.getProperty("user.dir"));
        //Path miDirectorio = ruta2;
        boolean cerramos = false;
                    
            System.out.println("David Molina [Versión 1.0.0.]\n" +
"(c) 2019 David Molina. Todos los derechos reservados.");
            
        while(!cerramos){
            
            String tp = " ";
            Scanner sc = new Scanner(System.in);
            tp = sc.nextLine();
            String [] comprobacion = tp.split("  ");
            String [] filename = tp.split(" ");
            
            switch(tp){
                
                case "cd":
                    Consola.cd(tp, ruta2);
                    break;
                case "help":
                    Consola.help(ruta2);//funciona
                    break;
                case "close":
                    Consola.close(tp, ruta2);//funciona
                    break;
                case "clear":
                    Consola.clear();//funciona
                    break;
                case "info":
                    Consola.info(ruta2);//funciona
                    break;
                case "mkdir":
                    Consola.mkDir(tp, ruta2);//funciona
                    break;
                case "mkfile":
                    Consola.mkFile(tp, comprobacion);//funciona
                    break;
                case "write":
                    Consola.write(tp, comprobacion);//funciona
                    break;
                case "delete":
                    Consola.delete(tp); //funciona
                    break;
                case "cat":
                    Consola.cat(tp); //funciona
                    break;
                case "top":
                    Consola.top(tp, tp);//funciona
                    break;
                case "dir":                     
                    Consola.dir(tp); //funciona a medias
                    break;
                case "borrarDirectorio":  //Para borrar el directorio hay que introducir la ruta entre comillas en el lugar del tp
                    Consola.borradoDeDirectorio(new File(tp)); //funciona pixi pixa a ver si te vale
                    break;
            }
        }
        
    }
    
}
